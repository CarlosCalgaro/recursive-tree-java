package sample;


import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;


public class RecursiveTree {

    private int x1;
    private int y1;
    private int height = 10;

    private Pane pane;
    private static final int DEPTH = 5;
    RecursiveTree(int posX, int posY){
        this.x1 = posX;
        this.y1 = posY;
        this.pane = new Pane();
        this.pane.setMinSize(500, 500);
    }



    public Pane draw(){
        int x2 = x1;
        int y2 = y1 - height;
        drawLine(x1, y1, x2, y2);
        branch(x2, y2, -90, DEPTH);
        return pane;
    }


    public void branch(int x1, int y1, double angle, int depth){
        if(depth == 0) return;
        int x2 = x1 + (int)(Math.cos(Math.toRadians(angle)) * depth * 2.5);
        int y2 = y1 + (int)(Math.sin(Math.toRadians(angle)) * depth * 2.5);
        drawLine(x1, y1, x2, y2);
        branch(x2, y2, angle - 20 , depth -1);
        branch(x2, y2, angle + 20, depth -1);
    }



    private void drawLine( int x1, int y1, int x2, int y2){
        Line line = new Line();
        line.setStartX(x1);
        line.setStartY(y1);
        line.setEndX(x2);
        line.setEndY(y2);
        line.setStroke(Color.GREEN);
        pane.getChildren().add(line);
    }

}
